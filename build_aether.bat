@echo off
title Updating to latest commit
cd The-Aether
git pull
title Building the Aether
call gradlew.bat build
title Copying Files
cd ..
xcopy "The-Aether\build\libs\aether-1.19.4*" "libs"
pause