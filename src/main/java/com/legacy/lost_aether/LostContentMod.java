package com.legacy.lost_aether;

import com.aetherteam.aether.client.renderer.accessory.GlovesRenderer;
import com.legacy.lost_aether.client.LCResourcePackHandler;
import com.legacy.lost_aether.client.LostContentItemModelPredicates;
import com.legacy.lost_aether.client.render.SentryShieldRenderer;
import com.legacy.lost_aether.data.LCDataGen;
import com.legacy.lost_aether.event.LCClientEvents;
import com.legacy.lost_aether.event.LCEvents;
import com.legacy.lost_aether.network.PacketHandler;
import com.legacy.lost_aether.registry.LCArmorTrims;
import com.legacy.lost_aether.registry.LCBiomeModifiers;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCFeatures;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCStructures;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import top.theillusivec4.curios.api.client.CuriosRendererRegistry;

@Mod(LostContentMod.MODID)
public class LostContentMod
{
	public static final String NAME = "Aether: Lost Content";
	public static final String MODID = "lost_aether_content";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public LostContentMod()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, LostContentConfig.COMMON_SPEC);
		ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, LostContentConfig.WORLD_SPEC);

		IEventBus mod = FMLJavaModLoadingContext.get().getModEventBus();
		IEventBus forge = MinecraftForge.EVENT_BUS;

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, LostContentConfig.CLIENT_SPEC);

			mod.addListener(LostContentMod::clientInit);
			mod.addListener(LCClientEvents::registerOverlays);
			mod.addListener(com.legacy.lost_aether.registry.LCParticles.Register::registerParticleFactories);
			mod.addListener(LCResourcePackHandler::packRegistry);
			mod.addListener(LCArmorTrims::registerSpriteData);
		});

		RegistrarHandler.registerHandlers(MODID, mod, LCFeatures.Configured.HANDLER, LCFeatures.Placed.HANDLER, LCBiomeModifiers.HANDLER, LCArmorTrims.PATTERN_HANDLER);

		mod.addListener(LostContentMod::commonInit);
		mod.register(LostContentRegistry.class);

		mod.addListener(LCEntityTypes::onAttributesRegistered);
		mod.addListener(EventPriority.LOWEST, LCEntityTypes::registerSpawnConditions);

		forge.register(LCEvents.class);

		LCStructures.init();
		RegistrarHandler.registerHandlers(MODID, mod);

		mod.register(LCDataGen.class);
	}

	public static void clientInit(FMLClientSetupEvent event)
	{
		MinecraftForge.EVENT_BUS.register(LCClientEvents.class);

		event.enqueueWork(() ->
		{
			LostContentItemModelPredicates.init();

			CuriosRendererRegistry.register(LCItems.power_gloves, GlovesRenderer::new);
			CuriosRendererRegistry.register(LCItems.sentry_shield, SentryShieldRenderer::new);
		});
	}

	public static void commonInit(FMLCommonSetupEvent event)
	{
		PacketHandler.register();
	}
}