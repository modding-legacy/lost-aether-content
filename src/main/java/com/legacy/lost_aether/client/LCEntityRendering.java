package com.legacy.lost_aether.client;

import com.aetherteam.aether.client.renderer.AetherModelLayers;
import com.aetherteam.aether.client.renderer.entity.FlyingCowRenderer;
import com.aetherteam.aether.client.renderer.entity.MoaRenderer;
import com.aetherteam.aether.client.renderer.entity.PhygRenderer;
import com.aetherteam.aether.client.renderer.entity.ValkyrieQueenRenderer;
import com.aetherteam.aether.client.renderer.entity.ValkyrieRenderer;
import com.aetherteam.aether.client.renderer.entity.model.CrystalModel;
import com.aetherteam.aether.client.renderer.entity.model.HaloModel;
import com.aetherteam.aether.client.renderer.entity.model.QuadrupedWingsModel;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.client.models.AerwhaleKingModel;
import com.legacy.lost_aether.client.models.AerwhaleModelOverride;
import com.legacy.lost_aether.client.models.FeatheredMoaModel;
import com.legacy.lost_aether.client.render.AerwhaleKingRenderer;
import com.legacy.lost_aether.client.render.CloudShotRenderer;
import com.legacy.lost_aether.client.render.FallingRockRenderer;
import com.legacy.lost_aether.client.render.layer.ColoredWingsBodyLayer;
import com.legacy.lost_aether.client.render.layer.ColoredWingsLayer;
import com.legacy.lost_aether.client.render.layer.MoaHeadFeatherLayer;
import com.legacy.lost_aether.client.render.layer.ValkyrieHaloLayer;
import com.legacy.lost_aether.registry.LCEntityTypes;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = LostContentMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class LCEntityRendering
{
	@SubscribeEvent
	public static void initLayers(final EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(LCRenderRefs.AERWHALE_KING, AerwhaleKingModel::createBodyLayer);
		/*event.registerLayerDefinition(LCRenderRefs.ZEPHYROO, ZephyrooModel::createBodyLayer);*/
		event.registerLayerDefinition(LCRenderRefs.FALLING_ROCK, CrystalModel::createBodyLayer);

		event.registerLayerDefinition(LCRenderRefs.VALKYRIE_HALO, () -> HaloModel.createLayer(0.0F, 0.0F, 0.0F, 0.0F));
		event.registerLayerDefinition(LCRenderRefs.VALKYRIE_QUEEN_HALO, () -> HaloModel.createLayer(0.0F, 0.0F, 0.0F, 0.0F));

		event.registerLayerDefinition(LCRenderRefs.MOA_FEATHERS, FeatheredMoaModel::createBodyLayer);
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public static void initPostLayers(final EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(AetherModelLayers.AERWHALE, AerwhaleModelOverride::createOverrideLayer);
	}

	@SubscribeEvent
	public static void initRenders(final EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(LCEntityTypes.AERWHALE_KING, AerwhaleKingRenderer::new);
		/*event.registerEntityRenderer(LCEntityTypes.ZEPHYROO, ZephyrooRenderer::new);*/
		event.registerEntityRenderer(LCEntityTypes.FALLING_ROCK, FallingRockRenderer::new);
		event.registerEntityRenderer(LCEntityTypes.CLOUD_SHOT, CloudShotRenderer::new);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@SubscribeEvent
	public static void addRenderLayers(EntityRenderersEvent.AddLayers event)
	{
		Minecraft mc = Minecraft.getInstance();
		for (EntityRenderer<?> renderer : mc.getEntityRenderDispatcher().renderers.values())
		{
			if (renderer instanceof PhygRenderer r)
			{
				r.addLayer(new ColoredWingsBodyLayer(r));
				r.addLayer(new ColoredWingsLayer(r, new QuadrupedWingsModel<>(mc.getEntityModels().bakeLayer(AetherModelLayers.PHYG_WINGS))));
			}

			if (renderer instanceof FlyingCowRenderer r)
			{
				r.addLayer(new ColoredWingsBodyLayer(r));
				r.addLayer(new ColoredWingsLayer(r, new QuadrupedWingsModel<>(mc.getEntityModels().bakeLayer(AetherModelLayers.FLYING_COW_WINGS))));
			}

			if (renderer instanceof ValkyrieRenderer r)
				r.addLayer(new ValkyrieHaloLayer(r, new HaloModel<>(mc.getEntityModels().bakeLayer(LCRenderRefs.VALKYRIE_HALO))));

			if (renderer instanceof ValkyrieQueenRenderer r)
				r.addLayer(new ValkyrieHaloLayer(r, new HaloModel<>(mc.getEntityModels().bakeLayer(LCRenderRefs.VALKYRIE_QUEEN_HALO))));

			if (renderer instanceof MoaRenderer r)
			{
				r.addLayer(new MoaHeadFeatherLayer<>(r, new FeatheredMoaModel<>(mc.getEntityModels().bakeLayer(LCRenderRefs.MOA_FEATHERS))));
			}
		}
	}

	/*@SubscribeEvent
	public static void addRenderLayers(EntityRenderersEvent.CreateSkullModels event)
	{
		event.registerSkullModel(null, null);
	}*/
}
