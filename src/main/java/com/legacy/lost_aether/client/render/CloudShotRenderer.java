package com.legacy.lost_aether.client.render;

import org.joml.Matrix3f;
import org.joml.Matrix4f;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.entity.CloudShotEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CloudShotRenderer<T extends CloudShotEntity> extends EntityRenderer<T>
{
	private static final ResourceLocation TEXTURE_LOCATION = LostContentMod.locate("textures/entity/aerwhale_king/cloud_shot.png");
	private static final RenderType RENDER_TYPE = RenderType.entityCutoutNoCull(TEXTURE_LOCATION);

	public CloudShotRenderer(EntityRendererProvider.Context pContext)
	{
		super(pContext);
	}

	@Override
	protected int getBlockLightLevel(T pEntity, BlockPos pPos)
	{
		return 15;
	}

	@Override
	public void render(T pEntity, float pEntityYaw, float pPartialTicks, PoseStack pMatrixStack, MultiBufferSource pBuffer, int pPackedLight)
	{
		pMatrixStack.pushPose();
		pMatrixStack.scale(1.5F, 1.5F, 1.5F);
		pMatrixStack.translate(0, 0.2F, 0);
		pMatrixStack.mulPose(this.entityRenderDispatcher.cameraOrientation());
		pMatrixStack.mulPose(Axis.YP.rotationDegrees(180.0F));
		PoseStack.Pose posestack$pose = pMatrixStack.last();
		Matrix4f matrix4f = posestack$pose.pose();
		Matrix3f matrix3f = posestack$pose.normal();
		VertexConsumer vertexconsumer = pBuffer.getBuffer(RENDER_TYPE);
		vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 0.0F, 0, 0, 1);
		vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 1.0F, 0, 1, 1);
		vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 1.0F, 1, 1, 0);
		vertex(vertexconsumer, matrix4f, matrix3f, pPackedLight, 0.0F, 1, 0, 0);
		pMatrixStack.popPose();
		super.render(pEntity, pEntityYaw, pPartialTicks, pMatrixStack, pBuffer, pPackedLight);
	}

	private static void vertex(VertexConsumer p_254095_, Matrix4f p_254477_, Matrix3f p_253948_, int p_253829_, float p_253995_, int p_254031_, int p_253641_, int p_254243_)
	{
		p_254095_.vertex(p_254477_, p_253995_ - 0.5F, (float) p_254031_ - 0.25F, 0.0F).color(255, 255, 255, 255).uv((float) p_253641_, (float) p_254243_).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(p_253829_).normal(p_253948_, 0.0F, 1.0F, 0.0F).endVertex();
	}

	@Override
	public ResourceLocation getTextureLocation(T pEntity)
	{
		return TEXTURE_LOCATION;
	}
}