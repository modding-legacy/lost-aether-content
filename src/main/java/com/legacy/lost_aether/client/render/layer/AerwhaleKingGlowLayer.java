package com.legacy.lost_aether.client.render.layer;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.client.models.AerwhaleKingModel;
import com.legacy.lost_aether.entity.AerwhaleKingEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.EyesLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class AerwhaleKingGlowLayer<T extends AerwhaleKingEntity, M extends AerwhaleKingModel<T>> extends EyesLayer<T, M>
{
	private static final ResourceLocation TEXTURE = LostContentMod.locate("textures/entity/aerwhale_king/aerwhale_king_glow.png");
	private static final RenderType RENDER_TYPE = RenderType.eyes(TEXTURE);

	public AerwhaleKingGlowLayer(RenderLayerParent<T, M> rendererIn)
	{
		super(rendererIn);
	}

	@Override
	public RenderType renderType()
	{
		return RENDER_TYPE;
	}

	@Override
	public void render(PoseStack pMatrixStack, MultiBufferSource pBuffer, int pPackedLight, T pLivingEntity, float pLimbSwing, float pLimbSwingAmount, float pPartialTicks, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
		if (pLivingEntity.getInvisAnimScale(pPartialTicks) < 0.2F)
			return;

		super.render(pMatrixStack, pBuffer, pPackedLight, pLivingEntity, pLimbSwing, pLimbSwingAmount, pPartialTicks, pAgeInTicks, pNetHeadYaw, pHeadPitch);
	}
}