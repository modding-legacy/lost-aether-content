 package com.legacy.lost_aether.client.render;

import com.aetherteam.aether.client.renderer.AetherModelLayers;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.registry.LCItems;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.PlayerModel;
import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.player.AbstractClientPlayer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.client.ICurioRenderer;

public class SentryShieldRenderer implements ICurioRenderer
{
	private static final ResourceLocation TEXTURE = tex(0);
	private static final ResourceLocation TEXTURE_SLIM = tex(1);
	private static final ResourceLocation TEXTURE_EMIS = tex(2);

	private final HumanoidModel<LivingEntity> shieldModel;
	private final PlayerModel<LivingEntity> shieldModelSlim;
	public final HumanoidModel<LivingEntity> shieldModelArm;
	/*public final PlayerModel<LivingEntity> shieldModelArmSlim;*/
	public final PlayerModel<LivingEntity> dummyArm;
	public final PlayerModel<LivingEntity> dummyArmSlim;

	public SentryShieldRenderer()
	{
		var models = Minecraft.getInstance().getEntityModels();
		this.shieldModel = new HumanoidModel<>(models.bakeLayer(AetherModelLayers.SHIELD_OF_REPULSION));
		this.shieldModelSlim = new PlayerModel<>(models.bakeLayer(AetherModelLayers.SHIELD_OF_REPULSION_SLIM), true);
		this.shieldModelArm = new HumanoidModel<>(models.bakeLayer(AetherModelLayers.SHIELD_OF_REPULSION_ARM));
		/*this.shieldModelArmSlim = new PlayerModel<>(models.bakeLayer(AetherModelLayers.SHIELD_OF_REPULSION_ARM_SLIM), true);*/
		this.dummyArm = new PlayerModel<>(models.bakeLayer(ModelLayers.PLAYER), false);
		this.dummyArmSlim = new PlayerModel<>(models.bakeLayer(ModelLayers.PLAYER_SLIM), true);
	}

	@Override
	public <T extends LivingEntity, M extends EntityModel<T>> void render(ItemStack stack, SlotContext slotContext, PoseStack poseStack, RenderLayerParent<T, M> renderLayerParent, MultiBufferSource buffer, int light, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		LivingEntity livingEntity = slotContext.entity();
		CuriosApi.getCuriosHelper().findFirstCurio(livingEntity, LCItems.sentry_shield).ifPresent((slotResult) -> CuriosApi.getCuriosHelper().getCuriosHandler(livingEntity).ifPresent(handler -> handler.getStacksHandler(slotResult.slotContext().identifier()).ifPresent(stacksHandler ->
		{
			if (stacksHandler.getRenders().get(slotResult.slotContext().index()))
			{
				ResourceLocation texture = TEXTURE;
				HumanoidModel<LivingEntity> model = this.shieldModel;

				if (livingEntity instanceof Player player && renderLayerParent.getModel()instanceof PlayerModel<?> playerModel)
				{
					if (playerModel.slim)
					{
						texture = TEXTURE_SLIM;
						model = this.shieldModelSlim;
					}
				}

				ICurioRenderer.followHeadRotations(slotContext.entity(), model.head);
				ICurioRenderer.followBodyRotations(slotContext.entity(), model);
				VertexConsumer consumer = ItemRenderer.getArmorFoilBuffer(buffer, RenderType.entityTranslucent(texture), false, false);
				model.renderToBuffer(poseStack, consumer, light, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
				
				model.renderToBuffer(poseStack, ItemRenderer.getArmorFoilBuffer(buffer, RenderType.eyes(TEXTURE_EMIS), false, false), light, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
			}
		})));
	}

	public void renderFirstPerson(ItemStack stack, PoseStack poseStack, MultiBufferSource buffer, int combinedLight, AbstractClientPlayer player, HumanoidArm arm)
	{
		boolean isSlim = player.getModelName().equals("slim");
		this.setupHand(/*isSlim ? this.dummyArmSlim : */this.dummyArm, poseStack, buffer, combinedLight, player, arm);
		this.setupShield(stack, /*isSlim ? this.shieldModelArmSlim : */this.shieldModelArm, poseStack, buffer, combinedLight, player, arm, isSlim);
	}

	private void setupShield(ItemStack stack, HumanoidModel<LivingEntity> model, PoseStack poseStack, MultiBufferSource buffer, int combinedLight, AbstractClientPlayer player, HumanoidArm arm, boolean isSlim)
	{
		this.setupModel(model, player);
		this.renderShield(arm == HumanoidArm.RIGHT ? model.rightArm : model.leftArm, poseStack, combinedLight, ItemRenderer.getArmorFoilBuffer(buffer, RenderType.entityTranslucent(isSlim ? TEXTURE_SLIM : TEXTURE), false, stack.isEnchanted()));
	}

	private void setupHand(PlayerModel<LivingEntity> model, PoseStack poseStack, MultiBufferSource buffer, int combinedLight, AbstractClientPlayer player, HumanoidArm arm)
	{
		this.setupModel(model, player);
		this.renderHand(arm == HumanoidArm.RIGHT ? model.rightArm : model.leftArm, model.rightSleeve, poseStack, combinedLight, buffer.getBuffer(RenderType.entityTranslucent(player.getSkinTextureLocation())));
	}

	private void setupModel(HumanoidModel<LivingEntity> model, AbstractClientPlayer player)
	{
		model.setAllVisible(false);
		model.attackTime = 0.0F;
		model.crouching = false;
		model.swimAmount = 0.0F;
		model.setupAnim(player, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
	}

	private void renderShield(ModelPart shieldArm, PoseStack poseStack, int combinedLight, VertexConsumer consumer)
	{
		shieldArm.visible = true;
		shieldArm.xRot = 0.0F;
		shieldArm.render(poseStack, consumer, combinedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
	}

	private void renderHand(ModelPart dummyArm, ModelPart dummySleeve, PoseStack poseStack, int combinedLight, VertexConsumer consumer)
	{
		dummyArm.visible = true;
		dummySleeve.visible = true;
		dummyArm.xRot = 0.0F;
		dummySleeve.xRot = 0.0F;
		dummyArm.render(poseStack, consumer, combinedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		dummySleeve.render(poseStack, consumer, combinedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
	}

	private static ResourceLocation tex(int type)
	{
		return LostContentMod.locate("textures/models/accessory/sentry_shield/sentry_shield_" + (type == 1 ? "slim_" : "") + "accessory" + (type == 2 ? "_glow" : "") + ".png");
	}
}
