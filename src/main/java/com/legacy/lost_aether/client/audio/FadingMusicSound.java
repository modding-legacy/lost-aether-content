package com.legacy.lost_aether.client.audio;

import net.minecraft.client.resources.sounds.AbstractTickableSoundInstance;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;

public class FadingMusicSound extends AbstractTickableSoundInstance
{
	public int ticksPlayed;

	public boolean shouldntFadeOut = true, forceStopped;
	public float requestedVolume = 1.0F;

	public FadingMusicSound(SoundEvent soundIn, SoundSource categoryIn, boolean repeat)
	{
		super(soundIn, categoryIn, SoundInstance.createUnseededRandom());
		this.looping = repeat;

		if (repeat)
			this.delay = 0;
		
		this.relative = true;
		this.volume = 0.05F;
	}

	@Override
	public void tick()
	{
		if (this.ticksPlayed >= 0)
		{
			if (shouldntFadeOut)
			{
				++this.ticksPlayed;
			}
			else
			{
				this.ticksPlayed -= 2;
			}

			this.ticksPlayed = Math.min(this.ticksPlayed, 40);
			this.volume = Math.max(0.0F, Math.min((float) this.ticksPlayed / 40.0F, Math.min(this.volume + (this.requestedVolume > this.volume ? 0.025F : -0.025F), 1.0F)));
		}
		else
		{
			this.stop();
		}
	}

	@Override
	public boolean isStopped()
	{
		return super.isStopped() || forceStopped;
	}

	public void setVolume(float vol)
	{
		this.volume = vol;
	}
	
	@Override
	public SoundInstance.Attenuation getAttenuation()
	{
		return SoundInstance.Attenuation.NONE;
	}
	
	public float getRealVolume()
	{
		return this.volume;
	}
}
