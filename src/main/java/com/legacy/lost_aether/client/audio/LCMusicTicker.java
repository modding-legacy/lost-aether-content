package com.legacy.lost_aether.client.audio;

import javax.annotation.Nullable;

import com.aetherteam.aether.AetherConfig;
import com.legacy.lost_aether.capability.player.LCPlayer;
import com.legacy.lost_aether.registry.LCSounds;

import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.texture.Tickable;
import net.minecraft.sounds.SoundSource;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LCMusicTicker implements Tickable
{
	public static final LCMusicTicker INSTANCE = new LCMusicTicker();
	public FadingMusicSound bossMusic, foggyBossMusic;

	public LCMusicTicker()
	{
	}

	@SuppressWarnings("resource")
	@Override
	public void tick()
	{
		if (AetherConfig.CLIENT.disable_aether_boss_music.get())
		{
			if (this.playingMusic())
			{
				this.stopMusic(this.bossMusic);
				this.stopMusic(this.foggyBossMusic);
			}

			return;
		}

		LocalPlayer player = mc().player;

		if (player != null)
		{
			LCPlayer.ifPresent(player, (lcPlayer) ->
			{
				boolean bossActive = lcPlayer.inKingFight();

				/*if (this.bossMusic != null && this.foggyBossMusic != null)
					player.displayClientMessage(Component.literal(bossActive + ": " + this.bossMusic + ": " + this.bossMusic.shouldntFadeOut + " / " + this.foggyBossMusic + ": " + this.foggyBossMusic.shouldntFadeOut), true);
				else
					player.displayClientMessage(Component.literal(bossActive + ": " + this.bossMusic + " / " + this.foggyBossMusic), true);*/

				// reset boss music to null if the volume is 0
				if (this.bossMusic != null && (this.bossMusic.getSound() == null || !this.bossMusic.shouldntFadeOut && this.bossMusic.getRealVolume() <= 0 || !mc().getSoundManager().isActive(this.bossMusic)))
					this.bossMusic = null;

				if (this.foggyBossMusic != null && (this.foggyBossMusic.getSound() == null || !this.foggyBossMusic.shouldntFadeOut && this.foggyBossMusic.getRealVolume() <= 0 || !mc().getSoundManager().isActive(this.foggyBossMusic)))
					this.foggyBossMusic = null;

				// Checking if there is a boss.
				if (bossActive/* && properDimension*/)
				{
					if (this.bossMusic != null && !this.bossMusic.shouldntFadeOut)
						this.bossMusic = null;

					if (this.foggyBossMusic != null && !this.foggyBossMusic.shouldntFadeOut)
						this.foggyBossMusic = null;

					/*for (FadingMusicSound music : new FadingMusicSound[] { this.bossMusic, this.foggyBossMusic })*/

					if (!this.playingMusic())
					{
						this.playBossMusic();

						if (lcPlayer.getFogTime() > 0)
						{
							this.bossMusic.setVolume(0.0001F);
							this.foggyBossMusic.setVolume(1.0F);
						}
					}
					else
					{
						boolean foggy = lcPlayer.getFogTime() > 0;
						this.bossMusic.requestedVolume = foggy ? 0.0F : 1.0F;
						this.foggyBossMusic.requestedVolume = !foggy ? 0.0F : 1.0F;
					}
				}
				else if (!bossActive && (mc().getSoundManager().isActive(this.bossMusic) || mc().getSoundManager().isActive(this.foggyBossMusic)))
				{
					this.stopMusic(this.bossMusic);
					this.stopMusic(this.foggyBossMusic);
				}
			});

			/*else
			{
				this.stopMusic(bossMusic);
				this.stopMusic(foggyBossMusic);
			}*/
		}
	}

	public boolean playingMusic()
	{
		return this.bossMusic != null && this.foggyBossMusic != null;
	}

	public void playBossMusic()
	{
		if (this.bossMusic != null)
			this.bossMusic.forceStopped = true;

		this.bossMusic = new FadingMusicSound(LCSounds.MUSIC_PLATINUM_BOSS, SoundSource.MUSIC, true);

		this.bossMusic.setVolume(1.0F);
		this.bossMusic.ticksPlayed = 40;

		mc().getSoundManager().play(this.bossMusic);

		if (this.foggyBossMusic != null)
			this.foggyBossMusic.forceStopped = true;

		this.foggyBossMusic = new FadingMusicSound(LCSounds.MUSIC_PLATINUM_BOSS_FOGGY, SoundSource.MUSIC, true);

		this.foggyBossMusic.setVolume(0.001F);
		this.foggyBossMusic.ticksPlayed = 40;
		this.foggyBossMusic.requestedVolume = 0.01F;

		mc().getSoundManager().play(this.foggyBossMusic);
	}

	/**
	 * Fades out music
	 */
	private void stopMusic(@Nullable FadingMusicSound sound)
	{
		if (sound != null)
			sound.shouldntFadeOut = false;
	}

	private static Minecraft mc()
	{
		return Minecraft.getInstance();
	}
}