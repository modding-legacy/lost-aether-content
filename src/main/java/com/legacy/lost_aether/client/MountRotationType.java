package com.legacy.lost_aether.client;

import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;

public enum MountRotationType
{
	NONE, CLASSIC, LEGACY, SMOOTH_LEGACY;

	public void applyRotations(LivingEntity mount, LivingEntity rider)
	{
		if (this == NONE)
			return;

		boolean isSmoothLegacy = this == SMOOTH_LEGACY;
		boolean isLegacy = this == LEGACY || isSmoothLegacy;

		double d01 = mount.getX() - rider.getX();
		double d2 = mount.getZ() - rider.getZ();
		float f = isLegacy ? mount.yBodyRotO + (rider.xxa < 0 ? -45F : 45F) : (float) (Mth.atan2(d2, d01) * Mth.RAD_TO_DEG) - 90.0F;

		if (isLegacy && rider.xxa != 0 || !isLegacy && (rider.zza != 0 || rider.xxa != 0))
			mount.setYBodyRot(Mth.rotLerp(0.6F, mount.yBodyRotO, updateRotation(mount.getYRot(), f, 40.0F)));
		else
			mount.setYBodyRot(Mth.rotLerp(isLegacy && !isSmoothLegacy ? 0.9F : 0.2F, mount.yBodyRotO, rider.getYRot()));
	}

	private static float updateRotation(float angle, float targetAngle, float maxIncrease)
	{
		float f = Mth.wrapDegrees(targetAngle - angle);

		if (f > maxIncrease)
			f = maxIncrease;

		if (f < -maxIncrease)
			f = -maxIncrease;

		return angle + f;
	}
}
