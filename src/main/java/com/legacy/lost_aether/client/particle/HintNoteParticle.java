package com.legacy.lost_aether.client.particle;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class HintNoteParticle extends MysticalNoteParticle
{
	public HintNoteParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(level, x, y, z, dx, dy, dz, spriteSet);

		this.alphaDecrease = 0.02F;
		this.scaleIncrease = 0.02F;
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		@Override
		public Particle createParticle(SimpleParticleType type, ClientLevel level, double x, double y, double z, double dx, double dy, double dz)
		{
			return new HintNoteParticle(level, x, y, z, dx, dy, dz, this.spriteSet);
		}
	}
}