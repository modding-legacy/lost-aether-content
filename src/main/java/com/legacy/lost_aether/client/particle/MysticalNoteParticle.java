package com.legacy.lost_aether.client.particle;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class MysticalNoteParticle extends TextureSheetParticle
{
	public float alphaDecrease = 0.003F, scaleIncrease = 0.007F;

	public MysticalNoteParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(level, x, y, z, dx, dy, dz);
		this.xd = dx;
		this.yd = dy;
		this.zd = dz;
		this.x = x;
		this.y = y;
		this.z = z;
		this.lifetime = (int) (7.0D / (Math.random() * 0.8D + 0.2D)) + 6;
		this.hasPhysics = false;
		this.quadSize = 0;
		this.setAlpha(0.5F);
		this.pickSprite(spriteSet);

		this.setColor(129F / 255F, 208F / 255F, (200F + (float) (Math.random() * 54F)) / 255F);
	}

	@Override
	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;
		if (this.alpha <= 0.0F && this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			if (!(this.alpha - 0.01F < 0.0F))
				this.setAlpha(this.alpha - this.alphaDecrease);

			this.move(this.xd, this.yd, this.zd);

			this.yd *= (double) 0.99F;

			if (this.quadSize < 0.2F)
				this.quadSize += this.scaleIncrease;
		}
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}

	@OnlyIn(Dist.CLIENT)
	public static class MysticalFactory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public MysticalFactory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		@Override
		public Particle createParticle(SimpleParticleType type, ClientLevel level, double x, double y, double z, double dx, double dy, double dz)
		{
			return new MysticalNoteParticle(level, x, y, z, dx, dy, dz, this.spriteSet);
		}
	}
}