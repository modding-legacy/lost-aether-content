package com.legacy.lost_aether.client.models;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.Entity;

public class FeatheredMoaModel<T extends Entity> extends EntityModel<T>
{
	protected final ModelPart head;
	public final ModelPart headFeathers;
	public final ModelPart leftFeather, topFeather, rightFeather;

	public FeatheredMoaModel(ModelPart root)
	{
		this.head = root.getChild("head");
		this.headFeathers = root.getChild("head_feathers");

		this.leftFeather = this.headFeathers.getChild("left_feather");
		this.topFeather = this.headFeathers.getChild("top_feather");
		this.rightFeather = this.headFeathers.getChild("right_feather");

	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 26).addBox(-4.0F, -8.0F, -12.0F, 8.0F, 8.0F, 16.0F), PartPose.offset(0.0F, 8.0F, -4.0F));

		PartDefinition head_feathers = partdefinition.addOrReplaceChild("head_feathers", CubeListBuilder.create(), PartPose.offset(0.0F, 8.0F, -4.0F));

		head_feathers.addOrReplaceChild("left_feather", CubeListBuilder.create().texOffs(118, 4).addBox(0.0F, -5.5F, 0.0F, 5.0F, 10.0F, 0.0F), PartPose.offset(4.0F, -3.5F, 4.0F));
		head_feathers.addOrReplaceChild("top_feather", CubeListBuilder.create().texOffs(108, 0).addBox(-5.0F, -4.0F, 0.0F, 10.0F, 4.0F, 0.0F), PartPose.offset(0.0F, -8.0F, 4.0F));
		head_feathers.addOrReplaceChild("right_feather", CubeListBuilder.create().texOffs(118, 4).mirror().addBox(-5.0F, -5.5F, 0.0F, 5.0F, 10.0F, 0.0F).mirror(false), PartPose.offset(-4.0F, -3.5F, 4.0F));

		return LayerDefinition.create(meshdefinition, 128, 64);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.rightFeather.yRot = 1F;
		this.leftFeather.yRot = -1F;
		this.topFeather.xRot = -0.8F;

		float anim = 0.3F * limbSwingAmount;

		this.rightFeather.yRot += anim;
		this.leftFeather.yRot += -anim;
		this.topFeather.xRot += -anim;
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
	{
		/*head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);*/
		this.headFeathers.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}