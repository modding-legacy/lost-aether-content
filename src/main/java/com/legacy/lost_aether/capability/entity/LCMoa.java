package com.legacy.lost_aether.capability.entity;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.aetherteam.aether.entity.passive.Moa;
import com.legacy.lost_aether.registry.LCMoaTypes;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraftforge.common.capabilities.Capability;

public class LCMoa implements ILCMoa
{
	private Moa moa;
	private float speedMultiplier = 0.0F, speedMultiplierO;
	private float prevRot;

	public LCMoa()
	{
	}

	public LCMoa(Moa moa)
	{
		super();
		this.moa = moa;
	}

	@Override
	public Capability<ILCMoa> getDefaultInstance()
	{
		return ILCMoa.INSTANCE;
	}

	@Nullable
	public static ILCMoa get(Moa animal)
	{
		return LCMoa.getIfPresent(animal, wingedAnimal -> wingedAnimal, () -> null);
	}

	public static <E extends Moa> void ifPresent(E animal, Consumer<ILCMoa> action)
	{
		if (animal != null)
		{
			Optional<ILCMoa> optional = animal.getCapability(ILCMoa.INSTANCE).resolve();
			if (optional.isPresent())
				action.accept(optional.get());
		}
	}

	public static <E extends Moa, R> R getIfPresent(E animal, Function<ILCMoa, R> action, Supplier<R> elseSupplier)
	{
		if (animal != null)
		{
			Optional<ILCMoa> optional = animal.getCapability(ILCMoa.INSTANCE).resolve();
			if (optional.isPresent())
				return action.apply(optional.get());
		}
		return elseSupplier.get();
	}

	@Override
	public void serverTick()
	{
		this.speedMultiplierO = this.speedMultiplier;

		if (!this.moa.isVehicle())
			this.speedMultiplier = 0;

		if (!(this.moa.getFirstPassenger()instanceof LivingEntity rider) || !this.moa.level().isClientSide())
			return;

		if (this.moa.isVehicle() && this.hasSpeedMultiplier())
		{
			float diff = Math.abs(this.moa.getYHeadRot() - this.prevRot) * Mth.DEG_TO_RAD;

			float dist = Mth.abs(moa.walkDist - moa.walkDistO);
			boolean riderMoving = rider.zza > 0;

			boolean moving = dist >= 0.08F;

			if (this.moa.getYHeadRot() == this.prevRot && riderMoving && !(this.moa.isEntityOnGround() && !moving))
			{
				if (this.speedMultiplier < 1.0F)
					this.speedMultiplier += (rider.xxa != 0 ? 0.003F : 0.005F) * (this.moa.isEntityOnGround() ? 0.2F : 1);
			}
			else if (this.speedMultiplier > 0F)
				this.speedMultiplier -= 0.6F * (!riderMoving || this.moa.isEntityOnGround() && !moving ? 0.1F : diff < 0.06F ? 0.02F : diff * 1.2F);
		}
		else
			this.speedMultiplier = 0.0F;

		this.speedMultiplier = Mth.clamp(this.speedMultiplier, 0.0F, 1.0F);
		this.prevRot = this.moa.getYHeadRot();
	}

	@Override
	public void modifySpeed(CallbackInfoReturnable<Float> callback)
	{
		if (!this.getMoa().isVehicle() || !this.hasSpeedMultiplier())
			return;

		callback.setReturnValue(((moa.getSpeed() - 0.11F) + (0.11F * this.getSpeedMultiplier())) * 0.45F);
	}

	private static final String SPEED_MULTIPLIER_KEY = "SpeedMultiplier";

	@Override
	public CompoundTag writeAdditional(CompoundTag nbt)
	{
		nbt.putFloat(SPEED_MULTIPLIER_KEY, this.speedMultiplier);
		return nbt;
	}

	@Override
	public void read(CompoundTag nbt)
	{
		this.speedMultiplier = nbt.getFloat(SPEED_MULTIPLIER_KEY);
	}

	@Override
	public Moa getMoa()
	{
		return this.moa;
	}

	@Override
	public float getSpeedMultiplier()
	{
		return this.speedMultiplier;
	}

	@Override
	public float lerpedSpeedMultiplier(float partialTicks)
	{
		return Mth.lerp(partialTicks, this.speedMultiplierO, this.speedMultiplier);
	}

	@Override
	public boolean hasSpeedMultiplier()
	{
		return this.moa.getMoaType() == LCMoaTypes.ORANGE.get();
	}
}
