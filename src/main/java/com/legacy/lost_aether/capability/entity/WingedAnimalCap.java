package com.legacy.lost_aether.capability.entity;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.legacy.lost_aether.LostContentConfig;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.animal.Animal;
import net.minecraftforge.common.capabilities.Capability;

public class WingedAnimalCap implements IWingedAnimal
{
	private Animal animal;
	private byte wingTypeId = 0;

	public WingedAnimalCap()
	{
	}

	public WingedAnimalCap(Animal animal)
	{
		super();
		this.animal = animal;
	}

	@Override
	public Capability<IWingedAnimal> getDefaultInstance()
	{
		return IWingedAnimal.INSTANCE;
	}

	@Nullable
	public static IWingedAnimal get(Animal animal)
	{
		return WingedAnimalCap.getIfPresent(animal, wingedAnimal -> wingedAnimal, () -> null);
	}

	public static <E extends Animal> void ifPresent(E animal, Consumer<IWingedAnimal> action)
	{
		if (animal != null)
		{
			Optional<IWingedAnimal> optional = animal.getCapability(IWingedAnimal.INSTANCE).resolve();
			if (optional.isPresent())
				action.accept(optional.get());
		}
	}

	public static <E extends Animal, R> R getIfPresent(E animal, Function<IWingedAnimal, R> action, Supplier<R> elseSupplier)
	{
		if (animal != null)
		{
			Optional<IWingedAnimal> optional = animal.getCapability(IWingedAnimal.INSTANCE).resolve();
			if (optional.isPresent())
				return action.apply(optional.get());
		}
		return elseSupplier.get();
	}

	private static final String WING_TYPE_KEY = "WingType";

	@Override
	public CompoundTag writeAdditional(CompoundTag nbt)
	{
		nbt.putByte(WING_TYPE_KEY, this.getWingType());
		return nbt;
	}

	@Override
	public void read(CompoundTag nbt)
	{
		this.setWingType(nbt.getByte(WING_TYPE_KEY));
	}

	@Override
	public Animal getAnimal()
	{
		return this.animal;
	}

	@Override
	public byte getWingType()
	{
		return this.wingTypeId;
	}

	@Override
	public void setWingType(byte id)
	{
		this.wingTypeId = id;
	}
	
	@Override
	public void setWingType(WingType type)
	{
		this.setWingType((byte) type.ordinal());
	}
	
	public static enum WingType
	{
		GOLD, SILVER, BRONZE;
	}

	@Override
	public boolean shouldDisplayWings()
	{
		return this.getWingType() > 0 && LostContentConfig.WORLD.mutationWingColors.get();
	}
}
