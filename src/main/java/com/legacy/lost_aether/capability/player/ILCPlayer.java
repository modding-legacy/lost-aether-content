package com.legacy.lost_aether.capability.player;

import com.legacy.lost_aether.capability.IPersistentCapability;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.event.ViewportEvent.ComputeFogColor;
import net.minecraftforge.client.event.ViewportEvent.RenderFog;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;

public interface ILCPlayer extends IPersistentCapability<ILCPlayer>
{
	public static final Capability<ILCPlayer> INSTANCE = CapabilityManager.get(new CapabilityToken<>()
	{
	});

	void serverTick();

	void clientTick();

	void syncDataToClient();

	int getFogTime();

	void setFogTime(int time);

	Player getPlayer();

	void renderFog(RenderFog event);

	void modifyFogColor(ComputeFogColor event);

	Vec3 modifyAetherSkyColor(Vec3 pPos, float pPartialTick, Vec3 value);

	int getRiseTime();

	void setRiseTime(int value);
	
	boolean inKingFight();

	void setKingFight(boolean inFight);
}
