package com.legacy.lost_aether.block.util;

import com.legacy.lost_aether.registry.LCFeatures;

import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class HolidayTreeGrower extends AbstractTreeGrower
{
	@Override
	public ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource rand, boolean hasLargeHive)
	{
		return LCFeatures.Configured.HOLIDAY_TREE_CONF.getKey();
	}
}