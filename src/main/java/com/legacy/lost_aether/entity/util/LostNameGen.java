package com.legacy.lost_aether.entity.util;

import java.util.Random;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;

public class LostNameGen
{
	private static final Random rand = new Random();
	private static String[] whaleNamePrefix = { "Raf", "Ro", "Ye", "Si", "Schw", "Spla" };
	private static String[] whaleNameMiddix = { "inkl", "ch", "flo", "", "" };
	private static String[] whaleNameSuffix = { "dorf", "umps", "dul", "dum", "er" };

	private static MutableComponent whaleGen()
	{
		String result = "";

		result += whaleNamePrefix[rand.nextInt(whaleNamePrefix.length)];
		result += whaleNameMiddix[rand.nextInt(whaleNameMiddix.length)];
		result += whaleNameSuffix[rand.nextInt(whaleNameSuffix.length)];

		return Component.literal(result);
	}

	public static MutableComponent generateAerwhaleKingName()
	{
		return whaleGen().append(Component.translatable("gui.lost_aether_content.aerwhale_king.title"));
	}
}