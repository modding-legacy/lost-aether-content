package com.legacy.lost_aether.data;

import com.aetherteam.aether.block.AetherBlocks;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.data.loot_modifiers.AddCrystalSaplingsModifier;
import com.legacy.lost_aether.data.loot_modifiers.AddHolidaySaplingsModifier;

import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.data.PackOutput;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.storage.loot.predicates.AnyOfCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.MatchTool;
import net.minecraftforge.common.data.GlobalLootModifierProvider;

public class LCLootModifiers extends GlobalLootModifierProvider
{
	public LCLootModifiers(PackOutput output)
	{
		super(output, LostContentMod.MODID);
	}

	@Override
	protected void start()
	{
		add("add_crystal_saplings", new AddCrystalSaplingsModifier(new LootItemCondition[] { AnyOfCondition.anyOf(LootItemBlockStatePropertyCondition.hasBlockStateProperties(AetherBlocks.CRYSTAL_LEAVES.get()), LootItemBlockStatePropertyCondition.hasBlockStateProperties(AetherBlocks.CRYSTAL_FRUIT_LEAVES.get())).build(), MatchTool.toolMatches(ItemPredicate.Builder.item().of(Items.SHEARS)).invert().build() }));
		add("add_holiday_saplings", new AddHolidaySaplingsModifier(new LootItemCondition[] { AnyOfCondition.anyOf(LootItemBlockStatePropertyCondition.hasBlockStateProperties(AetherBlocks.HOLIDAY_LEAVES.get()), LootItemBlockStatePropertyCondition.hasBlockStateProperties(AetherBlocks.DECORATED_HOLIDAY_LEAVES.get())).build(), MatchTool.toolMatches(ItemPredicate.Builder.item().of(Items.SHEARS)).invert().build() }));
	}
}