package com.legacy.lost_aether.data;

import com.legacy.lost_aether.LostContentMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.Structure;

public class LCTags
{
	public static interface Blocks
	{
		public static void init()
		{
		}

		TagKey<Block> GALE_BLOCKS = tag("gale_stones");

		TagKey<Block> ALTAR_ENHANCER = tag("altar_enhancer");
		TagKey<Block> INCUBATOR_ENHANCER = tag("incubator_enhancer");
		TagKey<Block> FREEZER_ENHANCER = tag("freezer_enhancer");

		private static TagKey<Block> tag(String name)
		{
			return BlockTags.create(LostContentMod.locate(name));
		}
	}

	public static interface Items
	{
		public static void init()
		{
		}

		TagKey<Item> GALE_STONES = tag("gale_stones");
		TagKey<Item> PHOENIX_TOOLS = tag("phoenix_tools");
		TagKey<Item> AETHER_SHIELDS = tag("aether_shields");
		TagKey<Item> PLATINUM_DUNGEON_LOOT = tag("platinum_dungeon_loot");

		private static TagKey<Item> tag(String name)
		{
			return ItemTags.create(LostContentMod.locate(name));
		}
	}

	public static interface Biomes
	{
		TagKey<Biome> HAS_PLATINUM_DUNGEON = tag("has_platinum_dungeon");
		TagKey<Biome> HAS_PINK_AERCLOUDS = tag("has_pink_aerclouds");

		private static TagKey<Biome> tag(String key)
		{
			return TagKey.create(Registries.BIOME, LostContentMod.locate(key));
		}

		public static void init()
		{
		}
	}

	public static interface Structures
	{
		TagKey<Structure> PLATINUM_DUNGEON_BAD_NEIGHBORS = tag("platinum_dungeon_bad_neighbors");

		private static TagKey<Structure> tag(String key)
		{
			return TagKey.create(Registries.STRUCTURE, LostContentMod.locate(key));
		}

		public static void init()
		{
		}
	}
}
