package com.legacy.lost_aether.network;

import java.util.function.Supplier;

import com.legacy.lost_aether.capability.player.LCPlayer;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

public record SyncPlayerPacket(CompoundTag nbt)
{
	public static void encoder(SyncPlayerPacket packet, FriendlyByteBuf buff)
	{
		buff.writeNbt(packet.nbt);
	}

	public static SyncPlayerPacket decoder(FriendlyByteBuf buff)
	{
		return new SyncPlayerPacket(buff.readNbt());
	}

	public static void handler(SyncPlayerPacket packet, Supplier<NetworkEvent.Context> context)
	{
		context.get().enqueueWork(() -> DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> handleClient(packet)));
		context.get().setPacketHandled(true);
	}

	@SuppressWarnings("resource")
	@OnlyIn(Dist.CLIENT)
	private static void handleClient(SyncPlayerPacket packet)
	{
		Player player = net.minecraft.client.Minecraft.getInstance().player;
		LCPlayer.ifPresent(player, lcPlayer -> lcPlayer.read(packet.nbt));
	}
}
