package com.legacy.lost_aether.item.util;

import java.util.function.Supplier;

import com.aetherteam.aether.AetherTags;

import net.minecraft.world.item.Tier;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.common.util.Lazy;

public enum LCItemTier implements Tier
{
	// @formatter:off
	PHOENIX(3, 1561, 8.0F, 4.0F, 12, () -> {return Ingredient.of(AetherTags.Items.PHOENIX_REPAIRING);});
	// @formatter:on

	private final int harvestLevel;

	private final int maxUses;

	private final float efficiency;

	private final float attackDamage;

	private final int enchantability;

	private final Lazy<Ingredient> repairMaterial;

	private LCItemTier(int harvestLevelIn, int maxUsesIn, float efficiencyIn, float attackDamageIn, int enchantabilityIn, Supplier<Ingredient> repairMaterialIn)
	{
		this.harvestLevel = harvestLevelIn;
		this.maxUses = maxUsesIn;
		this.efficiency = efficiencyIn;
		this.attackDamage = attackDamageIn;
		this.enchantability = enchantabilityIn;
		this.repairMaterial = Lazy.of(repairMaterialIn);
	}

	@Override
	public int getUses()
	{
		return this.maxUses;
	}

	@Override
	public float getSpeed()
	{
		return this.efficiency;
	}

	@Override
	public float getAttackDamageBonus()
	{
		return this.attackDamage;
	}

	@Override
	public int getLevel()
	{
		return this.harvestLevel;
	}

	@Override
	public int getEnchantmentValue()
	{
		return this.enchantability;
	}

	@Override
	public Ingredient getRepairIngredient()
	{
		return this.repairMaterial.get();
	}
}