package com.legacy.lost_aether.item;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.RecordItem;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class LCMusicDiscItem extends RecordItem
{
	public final String artistName;
	public final String songName;

	public LCMusicDiscItem(SoundEvent soundIn, String artist, String song, int time, Item.Properties builder)
	{
		super(7, () -> soundIn, builder, time);

		this.artistName = artist;
		this.songName = song;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public MutableComponent getDisplayName()
	{
		return Component.literal(this.artistName + " - " + this.songName);
	}
}
