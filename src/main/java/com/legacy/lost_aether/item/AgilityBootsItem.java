package com.legacy.lost_aether.item;

import java.util.UUID;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.legacy.lost_aether.item.util.LCArmorItem;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;

public class AgilityBootsItem extends LCArmorItem
{
	private static final UUID BOOT_SPEED_UUID = UUID.fromString("dea8ee3c-b626-4d21-92ee-d742d722663d");
	private static final AttributeModifier ATTRIBUTE = new AttributeModifier(BOOT_SPEED_UUID, "Agility Boots modifier", 0.02D, AttributeModifier.Operation.ADDITION);

	public AgilityBootsItem(ArmorMaterial pMaterial, ArmorItem.Type pSlot, Properties pProperties)
	{
		super(pMaterial, pSlot, pProperties);
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot pEquipmentSlot)
	{
		Multimap<Attribute, AttributeModifier> map = HashMultimap.create();

		if (pEquipmentSlot == EquipmentSlot.FEET)
			map.put(Attributes.MOVEMENT_SPEED, ATTRIBUTE);

		return map;
	}

}
