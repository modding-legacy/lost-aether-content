package com.legacy.lost_aether.item;

import java.util.List;

import javax.annotation.Nullable;

import com.aetherteam.aether.AetherTags;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class LCShieldItem extends ShieldItem
{
	public LCShieldItem(Properties pProperties)
	{
		super(pProperties);
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> components, TooltipFlag flag)
	{
		super.appendHoverText(stack, level, components, flag);

		if (flag.isCreative() && stack.is(LCItems.shield_of_emile))
			components.add(LCItems.PLATINUM_DUNGEON_TOOLTIP);
	}

	@Override
	public boolean isValidRepairItem(ItemStack pStack, ItemStack pRepairCandidate)
	{
		if (this == LCItems.zanite_shield)
			return pRepairCandidate.is(AetherTags.Items.ZANITE_REPAIRING);

		if (this == LCItems.gravitite_shield)
			return pRepairCandidate.is(AetherTags.Items.GRAVITITE_REPAIRING);
		
		if (this == LCItems.shield_of_emile)
			return pRepairCandidate.is(AetherTags.Items.HAMMER_OF_KINGBDOGZ_REPAIRING);

		return super.isValidRepairItem(pRepairCandidate, pRepairCandidate);
	}
}
