package com.legacy.lost_aether.registry;

import com.aetherteam.aether.api.AetherMoaTypes;
import com.aetherteam.aether.api.registers.MoaType;
import com.legacy.lost_aether.LostContentMod;

import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.registries.RegisterEvent;

public class LCMoaTypes
{
	public static final Lazy<MoaType> BROWN = Lazy.of(() -> new MoaType(new MoaType.Properties().egg(() -> LCItems.brown_moa_egg).maxJumps(3).speed(0.2F).spawnChance(0).texture(LostContentMod.MODID, "textures/entity/moa/brown_moa.png")));
	public static final Lazy<MoaType> ORANGE = Lazy.of(() -> new MoaType(new MoaType.Properties().egg(() -> LCItems.orange_moa_egg).maxJumps(2).speed(0.3F).spawnChance(30).texture(LostContentMod.MODID, "textures/entity/moa/orange_moa.png")));

	public static void init(RegisterEvent event)
	{
		event.register(AetherMoaTypes.MOA_TYPE_REGISTRY_KEY, LostContentMod.locate("brown"), BROWN);
		event.register(AetherMoaTypes.MOA_TYPE_REGISTRY_KEY, LostContentMod.locate("orange"), ORANGE);
	}
}
