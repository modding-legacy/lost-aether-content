package com.legacy.lost_aether.registry;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.data.LCTags;
import com.legacy.lost_aether.world.structure.PlatinumDungeonPieces;
import com.legacy.lost_aether.world.structure.PlatinumDungeonStructure;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;
import com.legacy.structure_gel.api.structure.GridStructurePlacement.TaggedExclusionZone;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride.BoundingBoxType;

public class LCStructures
{
	// @formatter:off
	public static final StructureRegistrar<PlatinumDungeonStructure> PLATINUM_DUNGEON = StructureRegistrar.builder(LostContentMod.locate("platinum_dungeon"), () -> () -> PlatinumDungeonStructure.CODEC)
			.addPiece("main", () -> PlatinumDungeonPieces.Piece::new)
			.addPiece("stone_fill", () -> PlatinumDungeonPieces.IslandPiece::new)
			.pushStructure(PlatinumDungeonStructure::new)
				.biomes(LCTags.Biomes.HAS_PLATINUM_DUNGEON)
				.noSpawns(BoundingBoxType.STRUCTURE).popStructure()
			.placement((c) -> GridStructurePlacement.builder(40, 15, 0.6F).exclusionZone(new TaggedExclusionZone(c.lookup(Registries.STRUCTURE).getOrThrow(LCTags.Structures.PLATINUM_DUNGEON_BAD_NEIGHBORS), 8)).build(LCStructures.PLATINUM_DUNGEON.getRegistryName()))
			.build();
	// @formatter:on

	public static void init()
	{
	}
}
