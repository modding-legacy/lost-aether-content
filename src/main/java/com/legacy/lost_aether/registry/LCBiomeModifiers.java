package com.legacy.lost_aether.registry;

import java.util.List;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.data.LCTags;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.MobSpawnSettings.SpawnerData;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.world.BiomeModifier;
import net.minecraftforge.common.world.ForgeBiomeModifiers;
import net.minecraftforge.registries.ForgeRegistries;

public class LCBiomeModifiers
{
	public static final RegistrarHandler<BiomeModifier> HANDLER = RegistrarHandler.getOrCreate(ForgeRegistries.Keys.BIOME_MODIFIERS, LostContentMod.MODID);

	public static final Pointer<BiomeModifier> ADD_PINK_AERCLOUDS = HANDLER.createPointer("add_pink_aerclouds", c -> addFeature(c, Decoration.TOP_LAYER_MODIFICATION, LCTags.Biomes.HAS_PINK_AERCLOUDS, LCFeatures.Placed.PINK_AERCLOUD.getKey()));

	public static BiomeModifier addFeature(BootstapContext<?> bootstrap, GenerationStep.Decoration step, TagKey<Biome> spawnTag, ResourceKey<PlacedFeature> feature)
	{
		return new ForgeBiomeModifiers.AddFeaturesBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), HolderSet.direct(bootstrap.lookup(Registries.PLACED_FEATURE).getOrThrow(feature)), step);
	}

	public static BiomeModifier addSpawn(BootstapContext<?> bootstrap, TagKey<Biome> spawnTag, SpawnerData... data)
	{
		return new ForgeBiomeModifiers.AddSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), List.of(data));
	}

	public static BiomeModifier removeSpawns(BootstapContext<?> bootstrap, TagKey<Biome> spawnTag, HolderSet<EntityType<?>> entityTypes)
	{
		return new ForgeBiomeModifiers.RemoveSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), entityTypes);
	}
}
