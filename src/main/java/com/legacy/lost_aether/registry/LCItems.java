package com.legacy.lost_aether.registry;

import java.util.List;

import com.aetherteam.aether.item.AetherItems;
import com.aetherteam.aether.item.accessories.AccessoryItem;
import com.aetherteam.aether.item.accessories.cape.CapeItem;
import com.aetherteam.aether.item.accessories.gloves.GlovesItem;
import com.aetherteam.aether.item.miscellaneous.DungeonKeyItem;
import com.aetherteam.aether.item.miscellaneous.MoaEggItem;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.item.AgilityBootsItem;
import com.legacy.lost_aether.item.LCMusicDiscItem;
import com.legacy.lost_aether.item.LCShieldItem;
import com.legacy.lost_aether.item.util.CustomHeadArmorItem;
import com.legacy.lost_aether.item.util.LCArmorMaterials;
import com.legacy.lost_aether.item.util.LCItemTier;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextColor;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.SmithingTemplateItem;
import net.minecraft.world.item.SwordItem;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

public class LCItems
{
	public static final Component PLATINUM_DUNGEON_TOOLTIP = Component.translatable("gui.lost_aether_content.dungeon.platinum_dungeon").withStyle(Style.EMPTY.withItalic(true).withColor(TextColor.parseColor("#9db8d1")));
	public static final ResourceLocation PLATINUM_KEY_TYPE = LostContentMod.locate("platinum");

	public static MoaEggItem orange_moa_egg, brown_moa_egg;

	public static Item phoenix_pickaxe, phoenix_axe, phoenix_shovel, phoenix_sword, phoenix_hoe;

	public static Item phoenix_cape, sentry_shield, invincibility_gem, power_gloves, flaming_gemstone;

	public static Item agility_boots, swetty_mask;

	public static LCShieldItem zanite_shield, gravitite_shield, shield_of_emile;

	public static DungeonKeyItem platinum_key;

	public static Item music_disc_legacy, music_disc_sovereign_of_the_skies;
	
	public static Item noble_trim;

	private static RegisterEvent registryEvent;

	public static void init(RegisterEvent event)
	{
		registryEvent = event;

		orange_moa_egg = register("orange_moa_egg", new MoaEggItem(LCMoaTypes.ORANGE, LostContentMod.locate("orange"), 0xF3C288, new Item.Properties()));
		brown_moa_egg = register("brown_moa_egg", new MoaEggItem(LCMoaTypes.BROWN, LostContentMod.locate("brown"), 0x5d5726, new Item.Properties()));

		phoenix_shovel = register("phoenix_shovel", new ShovelItem(LCItemTier.PHOENIX, 1.5F, -3.0F, (new Item.Properties().rarity(AetherItems.AETHER_LOOT))));
		phoenix_pickaxe = register("phoenix_pickaxe", new PickaxeItem(LCItemTier.PHOENIX, 1, -2.8F, (new Item.Properties().rarity(AetherItems.AETHER_LOOT))));
		phoenix_axe = register("phoenix_axe", new AxeItem(LCItemTier.PHOENIX, 7.0F, -3.15F, (new Item.Properties().rarity(AetherItems.AETHER_LOOT))));
		phoenix_sword = register("phoenix_sword", new SwordItem(LCItemTier.PHOENIX, 5, -2.7F, (new Item.Properties().rarity(AetherItems.AETHER_LOOT))));
		phoenix_hoe = register("phoenix_hoe", new HoeItem(LCItemTier.PHOENIX, -3, 0.0F, new Item.Properties().rarity(AetherItems.AETHER_LOOT)));

		phoenix_cape = register("phoenix_cape", new CapeItem(LostContentMod.locate("phoenix_cape"), new Item.Properties().durability(10).rarity(AetherItems.AETHER_LOOT)));
		sentry_shield = register("sentry_shield", new AccessoryItem(new Item.Properties().durability(25).rarity(AetherItems.AETHER_LOOT)));
		invincibility_gem = register("invincibility_gem", new AccessoryItem(new Item.Properties().durability(100).rarity(AetherItems.AETHER_LOOT)));
		power_gloves = register("power_gloves", new GlovesItem(LCArmorMaterials.POWER, 1.25D, LostContentMod.locate("power_gloves"), () -> SoundEvents.ARMOR_EQUIP_DIAMOND, new Item.Properties().durability(1700).rarity(AetherItems.AETHER_LOOT)));
		flaming_gemstone = register("flaming_gemstone", new AccessoryItem(new Item.Properties().durability(70).rarity(AetherItems.AETHER_LOOT)));

		swetty_mask = register("swetty_mask", new CustomHeadArmorItem(LCArmorMaterials.SWETTY, new Item.Properties().rarity(AetherItems.AETHER_LOOT)));
		agility_boots = register("agility_boots", new AgilityBootsItem(LCArmorMaterials.AGILITY, ArmorItem.Type.BOOTS, new Item.Properties().rarity(AetherItems.AETHER_LOOT)));

		zanite_shield = register("zanite_shield", new LCShieldItem(new Item.Properties().durability(672)));
		gravitite_shield = register("gravitite_shield", new LCShieldItem(new Item.Properties().durability(1008)));
		shield_of_emile = register("shield_of_emile", new LCShieldItem(new Item.Properties().durability(1344).rarity(AetherItems.AETHER_LOOT)));

		platinum_key = register("platinum_key", new DungeonKeyItem(PLATINUM_KEY_TYPE, new Item.Properties().stacksTo(1).rarity(AetherItems.AETHER_LOOT)));

		music_disc_legacy = register("music_disc_legacy", new LCMusicDiscItem(LCSounds.RECORD_LEGACY, "Lachney", "Legacy", 5940, new Item.Properties().stacksTo(1).rarity(AetherItems.AETHER_LOOT)));
		music_disc_sovereign_of_the_skies = register("music_disc_sovereign_of_the_skies", new LCMusicDiscItem(LCSounds.RECORD_SOVEREIGN_OF_THE_SKIES, "Jesterguy", "Sovereign of the Skies", 222 * 20, new Item.Properties().stacksTo(1).rarity(AetherItems.AETHER_LOOT)));
		noble_trim = register("noble_armor_trim_smithing_template", SmithingTemplateItem.createArmorTrimTemplate(LCArmorTrims.Pattterns.NOBLE));
		
		registerBlockItems();

		registryEvent = null;
	}

	private static void registerBlockItems()
	{
		LCBlocks.blockItems.stream().forEach((block) ->
		{
			var props = new Item.Properties();

			if (block == LCBlocks.enchanted_pink_aercloud)
			{
				props.rarity(Rarity.RARE);
				props.stacksTo(16);
			}

			register(ForgeRegistries.BLOCKS.getKey(block).getPath(), new BlockItem(block, props));
		});
		LCBlocks.blockItems = List.of();
	}

	private static <T extends Item> T register(String name, T item)
	{
		registryEvent.register(Registries.ITEM, LostContentMod.locate(name), () -> item);
		return item;
	}
}
