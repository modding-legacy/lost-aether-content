package com.legacy.lost_aether.registry;

import java.util.List;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.structure_gel.api.events.RegisterArmorTrimTexturesEvent;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.Util;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.armortrim.TrimPattern;

public class LCArmorTrims
{
	public static final RegistrarHandler<TrimPattern> PATTERN_HANDLER = RegistrarHandler.getOrCreate(Registries.TRIM_PATTERN, LostContentMod.MODID).bootstrap(LCArmorTrims.Pattterns::trimBootstrap);

	public static interface Pattterns
	{
		ResourceKey<TrimPattern> NOBLE = PATTERN_HANDLER.key("noble");

		/**
		 * Add to Item Tag: TRIM_TEMPLATES
		 * 
		 * Add recipes
		 * 
		 * @param context
		 */
		private static void trimBootstrap(BootstapContext<TrimPattern> context)
		{
			register(context, NOBLE, LCItems.noble_trim);
		}

		public static List<ResourceKey<TrimPattern>> patterns()
		{
			return List.of(NOBLE);
		}

		private static void register(BootstapContext<TrimPattern> context, ResourceKey<TrimPattern> key, Item templateItem)
		{
			TrimPattern trimpattern = new TrimPattern(key.location(), BuiltInRegistries.ITEM.wrapAsHolder(templateItem), Component.translatable(Util.makeDescriptionId("trim_pattern", key.location())));
			context.register(key, trimpattern);
		}
	}

	public static void registerSpriteData(RegisterArmorTrimTexturesEvent event)
	{
		LCArmorTrims.Pattterns.patterns().forEach(pattern -> event.registerPatternSprite(pattern));
	}
}
