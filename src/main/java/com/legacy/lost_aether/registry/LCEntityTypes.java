package com.legacy.lost_aether.registry;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.entity.AerwhaleKingEntity;
import com.legacy.lost_aether.entity.CloudShotEntity;
import com.legacy.lost_aether.entity.FallingRockEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.event.entity.SpawnPlacementRegisterEvent;
import net.minecraftforge.event.entity.SpawnPlacementRegisterEvent.Operation;
import net.minecraftforge.registries.RegisterEvent;

public class LCEntityTypes
{
	public static final EntityType<AerwhaleKingEntity> AERWHALE_KING = buildEntity("aerwhale_king", EntityType.Builder.of(AerwhaleKingEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).sized(4.0F, 4.0F).clientTrackingRange(10).fireImmune());
	/*public static final EntityType<ZephyrooEntity> ZEPHYROO = buildEntity("zephyroo", EntityType.Builder.of(ZephyrooEntity::new, MobCategory.CREATURE).sized(0.9F, 1.7F));*/
	public static final EntityType<FallingRockEntity> FALLING_ROCK = buildEntity("falling_rock", EntityType.Builder.<FallingRockEntity>of(FallingRockEntity::new, MobCategory.MISC).sized(0.9F, 0.9F));
	public static final EntityType<CloudShotEntity> CLOUD_SHOT = buildEntity("cloud_shot", EntityType.Builder.<CloudShotEntity>of(CloudShotEntity::new, MobCategory.MISC).sized(0.9F, 0.9F));

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("aerwhale_king", AERWHALE_KING);
		/*register("zephyroo", ZEPHYROO);*/
		register("falling_rock", FALLING_ROCK);
		register("cloud_shot", CLOUD_SHOT);

		registerEvent = null;
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(LostContentMod.find(key));
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(AERWHALE_KING, AerwhaleKingEntity.registerAttributes().build());
		/*event.put(ZEPHYROO, ZephyrooEntity.registerAttributes().build());*/
		/*event.put(FALLING_ROCK, Mob.createMobAttributes().build());*/
	}

	public static void registerSpawnConditions(SpawnPlacementRegisterEvent event)
	{
		event.register(LCEntityTypes.AERWHALE_KING, SpawnPlacements.Type.NO_RESTRICTIONS, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, AerwhaleKingEntity::spawnRules, Operation.REPLACE);
		/*event.register(LCEntityTypes.ZEPHYROO, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Animal::checkAnimalSpawnRules, Operation.REPLACE);*/
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, LostContentMod.locate(name), () -> type);
	}
}
