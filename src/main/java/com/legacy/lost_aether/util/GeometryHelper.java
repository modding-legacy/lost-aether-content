package com.legacy.lost_aether.util;

import net.minecraft.core.Vec3i;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;

public final class GeometryHelper
{
	/**
	 * Calculates the distance between two 3D points
	 * 
	 * @param firstPos
	 * @param secondPos
	 * @return
	 */
	public static double calcDistance(Vec3 firstPos, Vec3 secondPos)
	{
		double x = calcExponent(firstPos.x() - secondPos.x());
		double y = calcExponent(firstPos.y() - secondPos.y());
		double z = calcExponent(firstPos.z() - secondPos.z());
		double dist = Math.sqrt(x + y + z);

		return dist;
	}

	/**
	 * Calculates the distance between two 2D points
	 * 
	 * @param x1
	 * @param z1
	 * @param x2
	 * @param z2
	 * @return
	 */
	public static double calcDistance(int x1, int z1, int x2, int z2)
	{
		double x = calcExponent(x2 - x1);
		double z = calcExponent(z2 - z1);
		double dist = Math.sqrt(x + z);

		return dist;
	}

	public static double calcAngleRad(double arcLength, double radius, double multiplier)
	{
		if (radius == 0)
			radius = 0.0001;
		double angle = (arcLength * multiplier) / radius;
		return angle;
	}

	/**
	 * Calculates the exponent of a number.
	 * 
	 * @param i
	 * @return
	 */
	public static double calcExponent(double i)
	{
		return i * i;
	}

	public static boolean isPointInsideRec(Vec3i pos, Vec3i minPos, Vec3i maxPos)
	{
		return pos.getX() >= minPos.getX() && pos.getX() <= maxPos.getX() && pos.getZ() >= minPos.getZ() && pos.getZ() <= maxPos.getZ() && pos.getY() >= minPos.getY() && pos.getY() <= maxPos.getY();
	}

	/**
	 * The position passed is assumed to be centered at (0, 0, 0)
	 * 
	 * @param w Width of ellipsoid
	 * @param h Height of ellipsoid
	 * @param d Depth of ellipsoid
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public static boolean isInEllipsoid(double w, double h, double d, double x, double y, double z)
	{
		x = w - (x / 2);
		y = h - (y / 2);
		z = d - (z / 2);
		return (((x - w) * (x - w)) / w) + (((y - h) * (y - h)) / h) + (((z - d) * (z - d)) / d) < 1;
	}


	public static float clampDegrees(float angle, float targetAngle, float range)
	{
		float posMax = (angle < targetAngle) ? angle + 360 : angle;
		float posDist = Mth.wrapDegrees(posMax - targetAngle);
		if (Math.abs(posDist) <= range)
		{
			return angle;
		}

		float negMax = (targetAngle < angle) ? targetAngle + 360 : targetAngle;
		float negDist = Mth.wrapDegrees(negMax - angle);
		if (Math.abs(negDist) <= range)
		{
			return angle;
		}

		return posDist > negDist ? targetAngle + range : targetAngle - range;
	}
}
