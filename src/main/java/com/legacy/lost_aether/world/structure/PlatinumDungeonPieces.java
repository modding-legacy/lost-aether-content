package com.legacy.lost_aether.world.structure;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.joml.SimplexNoise;

import com.aetherteam.aether.block.AetherBlocks;
import com.aetherteam.aether.blockentity.TreasureChestBlockEntity;
import com.aetherteam.aether.data.resources.registries.AetherConfiguredFeatures;
import com.aetherteam.nitrogen.entity.BossRoomTracker;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.data.LCLootProv;
import com.legacy.lost_aether.entity.AerwhaleKingEntity;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCStructures;
import com.legacy.lost_aether.util.GeometryHelper;
import com.legacy.structure_gel.api.structure.GelTemplateStructurePiece;
import com.legacy.structure_gel.api.structure.processor.RemoveGelStructureProcessor;

import net.minecraft.core.BlockPos;
import net.minecraft.core.SectionPos;
import net.minecraft.core.Vec3i;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.StructurePiece;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockIgnoreProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class PlatinumDungeonPieces
{
	private static final ResourceLocation TOWER = locatePiece("tower");
	private static final ResourceLocation GROUND = locatePiece("ground");

	public static void assemble(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder builder, RandomSource random, RandomState randomState)
	{
		int xz = 96;
		int y = 30;
		builder.addPiece(new PlatinumDungeonPieces.IslandPiece(pos, new BlockPos(xz, y, xz), 0));

		// builder.addPiece(new PlatinumDungeonPieces.Piece(templateManager, GROUND,
		// pos, rotation));

		// new BlockPos(9, 21, 9)
		builder.addPiece(new PlatinumDungeonPieces.Piece(templateManager, TOWER, pos.offset(new BlockPos((xz / 2) - 14, y + 1, (xz / 2) - 14)/*.rotate(rotation)*/), rotation));
	}

	static ResourceLocation locatePiece(String location)
	{
		return LostContentMod.locate("platinum_dungeon/" + location);
	}

	public static class Piece extends GelTemplateStructurePiece
	{
		public Piece(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("main").get(), 0, structureManager, name, Piece.getPlacementSettings(structureManager, name, pos, rotation), pos);
			this.rotation = rotation;
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("main").get(), nbt, context.structureTemplateManager(), name -> Piece.getPlacementSettings(context.structureTemplateManager(), name, new BlockPos(nbt.getInt("TPX"), nbt.getInt("TPY"), nbt.getInt("TPZ")), Rotation.valueOf(nbt.getString("Rot"))));
		}

		private static StructurePlaceSettings getPlacementSettings(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			Vec3i sizePos = structureManager.get(name).get().getSize();
			BlockPos centerPos = new BlockPos(sizePos.getX() / 2, 0, sizePos.getZ() / 2);

			StructurePlaceSettings placementSettings = new StructurePlaceSettings().setKeepLiquids(false).setRotationPivot(centerPos).setRotation(rotation).setMirror(Mirror.NONE);
			placementSettings.addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK);
			placementSettings.addProcessor(RemoveGelStructureProcessor.INSTANCE);

			return placementSettings;
		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			if (originalState.getBlock() == Blocks.GREEN_WOOL)
				return rand.nextFloat() < 0.4F ? AetherBlocks.AETHER_DIRT_PATH.get().defaultBlockState() : AetherBlocks.AETHER_GRASS_BLOCK.get().defaultBlockState();

			// Replace Gale Stone randomly with the light variant
			if (originalState.getBlock() == LCBlocks.locked_gale_stone && rand.nextFloat() < 0.05F)
				return LCBlocks.locked_light_gale_stone.defaultBlockState();

			if (rand.nextFloat() < 0.005F/* && level.isEmptyBlock(pos.above())*/)
			{
				if (originalState.getBlock() == LCBlocks.locked_gale_stone)
					return LCBlocks.trapped_gale_stone.defaultBlockState();
				else if (originalState.getBlock() == LCBlocks.locked_light_gale_stone)
					return LCBlocks.trapped_light_gale_stone.defaultBlockState();
			}

			return originalState;
		}

		// create
		@Override
		public void postProcess(WorldGenLevel level, StructureManager structureManager, ChunkGenerator chunkGeneratorIn, RandomSource randomIn, BoundingBox bounds, ChunkPos chunkPosIn, BlockPos pos)
		{
			super.postProcess(level, structureManager, chunkGeneratorIn, randomIn, bounds, chunkPosIn, pos);
		}

		@Override
		protected void handleDataMarker(String function, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox sbb)
		{
			if (function.contains("boss"))
			{
				AerwhaleKingEntity entity = new AerwhaleKingEntity(LCEntityTypes.AERWHALE_KING, level.getLevel());
				entity.setPos((double) pos.getX() - 16.0D, (double) pos.getY() + 12.0D, (double) pos.getZ());

				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);

				entity.setDungeon(new BossRoomTracker<AerwhaleKingEntity>(entity, Vec3.atBottomCenterOf(pos), new AABB(pos).inflate(20, 18, 20).move(0, 8 + 9, 0), new ArrayList<>()));
				entity.setYHeadRot(180);

				level.addFreshEntity(entity);
			}
			else if (function.contains("platinum_chest"))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);

				if (level.getBlockEntity(pos.below()) instanceof TreasureChestBlockEntity be)
				{
					be.setLootTable(LCLootProv.PLATINUM_TREASURE_LOOT, rand.nextLong());
					be.setKind(LCItems.PLATINUM_KEY_TYPE);
				}
			}
			else if (function.contains("loot_chest"))
			{
				BlockPos blockpos = pos.below();
				BlockEntity tile = level.getBlockEntity(blockpos);

				if (tile instanceof ChestBlockEntity chest)
				{
					if (rand.nextFloat() < 0.2F)
					{
						if (level.getBlockState(blockpos).hasProperty(ChestBlock.FACING))
							level.setBlock(blockpos, AetherBlocks.CHEST_MIMIC.get().defaultBlockState().setValue(ChestBlock.FACING, level.getBlockState(blockpos).getValue(ChestBlock.FACING)), 3);
					}
					else
						chest.setLootTable(LCLootProv.PLATINUM_DUNGEON_LOOT, rand.nextLong());
				}

				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
			}
		}
	}

	public static class IslandPiece extends StructurePiece
	{
		protected IslandPiece(BlockPos pos, BlockPos size, int componentType)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("stone_fill").get(), componentType, BoundingBox.fromCorners(pos, pos.offset(size)));
		}

		public IslandPiece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("stone_fill").get(), nbt);
		}

		@Override
		protected void addAdditionalSaveData(StructurePieceSerializationContext context, CompoundTag nbt)
		{
		}

		@Override
		public void postProcess(WorldGenLevel level, StructureManager structureTemplateManager, ChunkGenerator chunkGen, RandomSource rand, BoundingBox chunkBounds, ChunkPos chunkPos, BlockPos pos)
		{
			/*this.generateBox(world, bounds, this.boundingBox.minX(), this.boundingBox.minY(), this.boundingBox.minZ(), this.boundingBox.maxX(), this.boundingBox.maxY(), this.boundingBox.maxZ(), AetherBlocks.AETHER_DIRT.get().defaultBlockState(), AetherBlocks.AETHER_DIRT.get().defaultBlockState(), false);*/

			BlockState baseMaterial = AetherBlocks.HOLYSTONE.get().defaultBlockState();
			BlockState topMaterial = AetherBlocks.AETHER_GRASS_BLOCK.get().defaultBlockState();
			BlockState soilMaterial = AetherBlocks.AETHER_DIRT.get().defaultBlockState();

			// Noise scaling
			float horizontalNoiseScale = 3F;
			float verticalNoiseScale = 5F;
			// How blobby the island is
			int deltaDist = 4;
			// How cubic the blob should be. Used to get closer to the beta terrain shape
			float dicingAmount = 0.3F;

			BoundingBox bb = this.getBoundingBox();
			int maxDist = bb.getXSpan() / 2 - deltaDist;
			Vec3 originVec = pos.getCenter(); // Center position
			Set<BlockPos> positions = new HashSet<>();
			for (int x = chunkBounds.minX(); x <= chunkBounds.maxX(); x++)
			{
				for (int z = chunkBounds.minZ(); z <= chunkBounds.maxZ(); z++)
				{
					float heightScale = 0.03F;
					int maxY = bb.maxY() + (int) ((SimplexNoise.noise(x * heightScale, z * heightScale) + 1) * 2);

					for (int y = bb.minY(); y <= maxY; y++)
					{
						BlockPos placePos = new BlockPos(x, y, z);
						Vec3 vec = placePos.getCenter();
						int dicing = 9;
						Vec3 dicedVec8 = new BlockPos(x / dicing * dicing, y / dicing * dicing, z / dicing * dicing).getCenter();

						// Get the actual distance between the point and structureOrigin. If it's beyond
						// the max dist, we can skip it to help compute time
						float dist = (float) vec.distanceTo(originVec);
						if (dist > maxDist + deltaDist)
							continue;

						// Get the noise value based on the normal vector from pos to origin, offset by
						// structureOrigin
						Vec3[] vecs = new Vec3[] { vec, dicedVec8 };
						float[] noises = new float[vecs.length];
						for (int i = 0; i < vecs.length; i++)
						{
							Vec3 v = vecs[i];
							Vec3 angle = originVec.subtract(v).normalize().add(originVec);
							noises[i] = SimplexNoise.noise((float) angle.x() * horizontalNoiseScale, (float) angle.y() * verticalNoiseScale, (float) angle.z() * horizontalNoiseScale);
						}

						float n = noises[0] * (1 - dicingAmount) + noises[1] * dicingAmount;

						// Scale the final result based on yLevel to round off the bottom
						float yScaling = (y - bb.minY()) / (float) bb.getYSpan();
						float scale;
						if (yScaling < 0.8335)
							scale = 1 - (float) Math.pow(1 - yScaling, 5);
						else
							scale = (float) Math.sin(yScaling * 2.0);

						// The value dist is compared to, taking noise into account.
						float threshold = (maxDist + (deltaDist * n)) * scale;

						// Store the position as one to place
						if (dist < threshold)
						{
							positions.add(placePos);
						}
					}
				}
			}

			// Actually place the blocks
			if (positions.isEmpty())
				return;

			ChunkAccess chunk = level.getChunk(new BlockPos(chunkBounds.minX(), chunkBounds.minY(), chunkBounds.minZ()));
			Heightmap surfaceHM = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.WORLD_SURFACE_WG);
			Heightmap oceanHM = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.OCEAN_FLOOR_WG);

			ConfiguredFeature<?, ?> configuredFeature = level.registryAccess().registryOrThrow(Registries.CONFIGURED_FEATURE).get(AetherConfiguredFeatures.CRYSTAL_TREE_CONFIGURATION);

			for (BlockPos placePos : positions)
			{
				BlockState state = baseMaterial;
				if (!positions.contains(placePos.above(1)))
					state = topMaterial;
				else if (!positions.contains(placePos.above(2)))
					state = soilMaterial;
				else if (!positions.contains(placePos.above(3)) && rand.nextBoolean())
					state = soilMaterial;

				level.setBlock(placePos, state, Block.UPDATE_CLIENTS);
				BlockPos hmPos = new BlockPos(SectionPos.sectionRelative(placePos.getX()), placePos.getY() + 1, SectionPos.sectionRelative(placePos.getZ()));
				surfaceHM.update(hmPos.getX(), hmPos.getY(), hmPos.getZ(), state);
				oceanHM.update(hmPos.getX(), hmPos.getY(), hmPos.getZ(), state);

				if (configuredFeature != null && state == topMaterial && rand.nextFloat() < 0.03F && GeometryHelper.calcDistance(this.boundingBox.getCenter().getX(), this.boundingBox.getCenter().getZ(), placePos.getX(), placePos.getZ()) > 18)
					configuredFeature.place(level, level.getLevel().getChunkSource().getGenerator(), rand, placePos.above());
			}
		}
	}
}
