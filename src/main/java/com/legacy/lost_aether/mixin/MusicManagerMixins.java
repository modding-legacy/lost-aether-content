package com.legacy.lost_aether.mixin;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.aetherteam.aether.AetherConfig;
import com.aetherteam.aether.client.AetherMusicManager;
import com.legacy.lost_aether.client.audio.LCMusicTicker;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.client.sounds.MusicManager;

public class MusicManagerMixins
{
	@Mixin(MusicManager.class)
	public static class Vanilla
	{
		@Shadow
		@Final
		private Minecraft minecraft;

		@Nullable
		@Shadow
		private SoundInstance currentMusic;

		@Shadow
		private int nextSongDelay;

		@Inject(at = @At("HEAD"), method = "tick", cancellable = true)
		private void tick(CallbackInfo callback)
		{
			if (!AetherConfig.CLIENT.disable_aether_boss_music.get() && LCMusicTicker.INSTANCE.playingMusic())
			{
				if (this.currentMusic != null)
				{
					this.minecraft.getSoundManager().stop(this.currentMusic);
					this.currentMusic = null;
				}

				this.nextSongDelay = 300;
				callback.cancel();
			}
		}
	}

	@Mixin(AetherMusicManager.class)
	public static class Aether
	{
		@Shadow(remap = false)
		private static int nextSongDelay;

		@Inject(at = @At("HEAD"), method = "tick", cancellable = true, remap = false)
		private static void tick(CallbackInfo callback)
		{
			if (!AetherConfig.CLIENT.disable_aether_boss_music.get() && LCMusicTicker.INSTANCE.playingMusic())
			{
				if (AetherMusicManager.getCurrentMusic() != null)
					AetherMusicManager.stopPlaying();

				callback.cancel();
			}
		}
	}
}
