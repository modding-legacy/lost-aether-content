package com.legacy.lost_aether.mixin;

import java.util.List;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * RegistrySetBuilder
 * 
 * This exists since Aether data stuff just doesn't exist for us in datagen.
 *
 */
@Mixin(targets = "net.minecraft.core.RegistrySetBuilder$BuildState")
public class RegistrySetBuilderMixin
{
	@Shadow
	private List<RuntimeException> errors;

	@Inject(at = @At("HEAD"), method = "throwOnError", cancellable = true)
	private void throwOnError(CallbackInfo callback)
	{
		if (!this.errors.isEmpty())
		{
			IllegalStateException illegalstateexception = new IllegalStateException("Errors during registry creation");
			System.out.println(illegalstateexception);

			this.errors.forEach(e -> System.err.println(e.getMessage()));
		}
		callback.cancel();
	}
}
