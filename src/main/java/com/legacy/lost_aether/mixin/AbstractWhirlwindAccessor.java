package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import com.aetherteam.aether.entity.monster.AbstractWhirlwind;

@Mixin(AbstractWhirlwind.class)
public interface AbstractWhirlwindAccessor
{
	@Accessor(value = "dropsTimer")
	void setDropsTimer(int timer);
}
