package com.legacy.lost_aether.mixin;

import javax.annotation.Nonnull;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.aetherteam.aether.client.renderer.entity.model.AerwhaleModel;
import com.aetherteam.aether.entity.passive.Aerwhale;
import com.legacy.lost_aether.LostContentConfig;
import com.legacy.lost_aether.client.models.AerwhaleModelOverride;

import net.minecraft.client.model.geom.ModelPart;

@Mixin(AerwhaleModel.class)
public class AerwhaleModelMixin
{
	@Shadow(remap = false)
	public ModelPart head;

	@Inject(at = @At("HEAD"), method = "setupAnim", cancellable = true, remap = false)
	private void setupAnim(@Nonnull Aerwhale aerwhale, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, CallbackInfo callback)
	{
		if (LostContentConfig.CLIENT.updatedAerwhaleAnimations.get())
			AerwhaleModelOverride.setupAnim(aerwhale, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, this.head);
	}
}
