package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

import com.legacy.lost_aether.client.render.AerwhaleKingRenderer;
import com.legacy.lost_aether.entity.AerwhaleKingEntity;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.world.entity.LivingEntity;

@Mixin(LivingEntityRenderer.class)
public class LivingEntityRendererMixin<T extends LivingEntity, M extends EntityModel<T>>
{
	@ModifyConstant(constant = @Constant(floatValue = 1.0F, ordinal = 6), method = "render")
	private float lost_contenr$modifyVisibility(float original, T entity, float entityYaw, float partialTicks)
	{
		if (entity instanceof AerwhaleKingEntity aerwhale)
			return AerwhaleKingRenderer.getVisibility(aerwhale, partialTicks);

		return original;
	}
}
